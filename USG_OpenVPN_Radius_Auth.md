Last Updated: 20/06/2020

## Details

I wanted to run an OpenVPN server on the USG, turns out it's not that easy.

Thanks to the following resources in helping to configure this:

- [appleguru on Ubnt.com forums](https://community.ubnt.com/t5/UniFi-Routing-Switching/How-To-OpenVPN-Server-Configuration-on-the-USG/td-p/1683424)
- [bartdevos on Ubnt.com forums](https://community.ubnt.com/t5/EdgeRouter/EdgeMax-ERPro-1-8-OpenVPN-server-with-Radius-authentication-for/m-p/2472524#M219975)
- [unifimynet on Ubnt.com forums](https://community.ubnt.com/t5/UniFi-Routing-Switching/HowTo-protect-your-custom-scripts-across-firmware-versions-run-a/td-p/2147592)
- [jcconnell on github](https://gist.github.com/jcconnell/ec3c942c818a571d97f5ceaf954a37b0)

My UniFi Controller runs on MX Linux (maybe in the future I will do a guide on how to install it since I've encountered problems with that) and I used the new GUI in the controller. To enable it you just have to go under **Setting > Try New Settings**.

Before you begin the configuration run this command:

`sudo mca-ctrl -t dump-cfg > /config/config-original.json`

It will be useful if you do things differently from me and you need to create your personal configuration with any diff tool.

## Configure the RADIUS server

First you will need to enable RADIUS Server, go under **Settings > Gateway > RADIUS**, set a password for the server and enable it.

Now you have to create the user you will use to connect with OpenVPN, just click on the button **Create New User** in the bottom right corner. Clicking on it will display a GUI where you will have to insert an username, a password for the user, a VLAN id if you want to connect the user to a vlan, the tunnel type which can be **1 - Point-to-Point Tunneling Protocol (PPTP)** or **3 - Layer Two Tunneling Protocol (L2TP)** (I've used **3**) and the Tunnel medium type **1 - IPv4 (IP version 4)**.

Also in the same page **enable tunneled reply**.

## Configure easy-rsa and generate keys

SSH into your USG and run the following commands

To SSH into your USG you will need the SSH Password, just go under **Settings > Network Settings > Device Authentication**

```
sudo bash
curl -O https://ftp.us.debian.org/debian/pool/main/e/easy-rsa/easy-rsa_2.2.2-2_all.deb
sudo dpkg -i easy-rsa_2.2.2-2_all.deb

# Generate Keys
cd /usr/share/easy-rsa
. vars
./clean-all
./build-ca

# Give it a name as you like
./build-key-server server

# Set the common name to “server”
# Answer yes to signing the certificate and commiting it.
./build-dh

# Copy the generated keys
mkdir /config/auth/keys/
cp keys/* /config/auth/keys/
```

## Configure OpenVPN on the USG

In the following lines we will allow the vpn to work on the USG by adding some rules.

I've configured the OpenVPN server to be on the subnet: **10.72.1.0/24**

You can choose a different subnet but it **MUST** be different from any other subnet in the network. Also, be aware that in the end when configuring the controller to make everything persistent you will have a different configuration.

```
configure
set interfaces openvpn vtun0 mode server

# Make sure to use a subnet not in use anywhere else on your USG
set interfaces openvpn vtun0 server subnet 10.72.1.0/24
set interfaces openvpn vtun0 tls ca-cert-file /config/auth/keys/ca.crt
set interfaces openvpn vtun0 tls cert-file /config/auth/keys/server.crt
set interfaces openvpn vtun0 tls key-file /config/auth/keys/server.key
set interfaces openvpn vtun0 tls dh-file /config/auth/keys/dh2048.pem
set interfaces openvpn vtun0 encryption aes128
set interfaces openvpn vtun0 openvpn-option "--keepalive 8 30"
set interfaces openvpn vtun0 openvpn-option "--comp-lzo"
set interfaces openvpn vtun0 openvpn-option "--duplicate-cn"
set interfaces openvpn vtun0 openvpn-option "--user nobody --group nogroup"
set interfaces openvpn vtun0 openvpn-option "--plugin /usr/lib/openvpn/openvpn-auth-pam.so openvpn"
set interfaces openvpn vtun0 openvpn-option "--client-cert-not-required --username-as-common-name"
set interfaces openvpn vtun0 openvpn-option "--verb 1"
set interfaces openvpn vtun0 openvpn-option "--proto udp6"
set interfaces openvpn vtun0 openvpn-option "--port 1194"
set interfaces openvpn vtun0 openvpn-option "--push redirect-gateway def1"
set interfaces openvpn vtun0 openvpn-option "--push dhcp-option DNS 1.1.1.1"
set interfaces openvpn vtun0 openvpn-option "--push dhcp-option DNS 8.8.8.8"

# Configure the firewall
set firewall name WAN_LOCAL rule 20 action accept
set firewall name WAN_LOCAL rule 20 description "Allow OpenVPN clients in"
set firewall name WAN_LOCAL rule 20 destination port 1194
set firewall name WAN_LOCAL rule 20 log disable
set firewall name WAN_LOCAL rule 20 protocol udp

# (Optional) Configure the firewall for IPv6
set firewall ipv6-name wan_local-6 rule 20 action accept
set firewall ipv6-name wan_local-6 rule 20 description "Allow OpenVPN clients in"
set firewall ipv6-name wan_local-6 rule 20 destination port 1194
set firewall ipv6-name wan_local-6 rule 20 log disable
set firewall ipv6-name wan_local-6 rule 20 protocol udp

# Forward traffic to the internet
set service nat rule 5010 description "Masquerade for WAN"
set service nat rule 5010 outbound-interface eth0
set service nat rule 5010 type masquerade

commit
save
exit
```

## Create a .ovpn file

Give this to your clients for connection. Make sure to change the relevant details

```
client
float
dev tun

# EDIT THIS HOSTNAME, 
#normally this is your public ip or a DyDNS pointing to your server
remote my.hostname.com 1194 udp

resolv-retry infinite
nobind
persist-key
persist-tun
auth-user-pass
cipher AES-128-CBC
comp-lzo
verb 3
<ca>
 -----BEGIN CERTIFICATE----- 
# put your certificate block here. Copy it from your /config/auth/keys/ca.crt file on your USG
-----END CERTIFICATE----- 
</ca> 

# this is an random certificate. The .ovpn file needs one, but doesn't use it, so you can leave this as is
<cert> 
-----BEGIN CERTIFICATE-----
MIIB1jCCAT+gAwIBAgIEAmLSTjANBgkqhkiG9w0BAQUFADAVMRMwEQYDVQQDEwpP
cGVuVlBOIENBMB4XDTEzMDExNzAyMTExMloXDTIzMDEyMjAyMTExMlowKDEmMCQG
A1UEAxQdZnJyaWN0aW9uQGdtYWlsLmNvbV9BVVRPTE9HSU4wgZ8wDQYJKoZIhvcN
AQEBBQADgY0AMIGJAoGBALVEXIZYYu1Inmejuo4Si6Eo5AguTX5sg1pGbLkJSTR4
BXQsy6ocUnZ9py8htYkipkUUhjY7zDu+wJlUtWnVCwCYtewYfEc/+azH7+7eU6ue
T2K2IKdik1KWhdtNbaNphVvSlgdyKiuZDTCedptgWyiL50N7FMcUUMjjXYh/hftB
AgMBAAGjIDAeMAkGA1UdEwQCMAAwEQYJYIZIAYb4QgEBBAQDAgeAMA0GCSqGSIb3
DQEBBQUAA4GBABhVzSYXHlQEPNaKGmx9hMwwnNKcHgD9cCmC9lX/KR2Y+vT/QGxK
7sYlJInb/xmpa5TUQYc1nzDs9JBps1mCtZbYNNDpYnKINAKSDsM+KOQaSYQ2FhHk
bmBZk/K96P7VntzYI5S02+hOWnvjq5Wk4gOt1+L18+R/XujuxGbwnHW2
-----END CERTIFICATE-----
</cert>

# this is an random key. The .ovpn file needs one, but doesn't use it, so you can leave this as is
<key>
-----BEGIN PRIVATE KEY-----
MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALVEXIZYYu1Inmej
uo4Si6Eo5AguTX5sg1pGbLkJSTR4BXQsy6ocUnZ9py8htYkipkUUhjY7zDu+wJlU
tWnVCwCYtewYfEc/+azH7+7eU6ueT2K2IKdik1KWhdtNbaNphVvSlgdyKiuZDTCe
dptgWyiL50N7FMcUUMjjXYh/hftBAgMBAAECgYEAsNjgOEYVRhEaUlzfzmpzhakC
SKT8AALYaAPbYO+ZVzJdh8mIbg+xuF7A9G+7z+5ZL35lrpXKnONuvmlxkK5ESwvV
Q7EOQYCZCqa8xf3li3GUBLwcwXKtOUr3AYXhdbOh2viQdisD4Ky7H6/Nd3yMc3bu
R4pErmWeHei+l6dIwAECQQDqljNxi9babmHiei6lHaznCMg5+jfAyDXgHvO/afFr
1bDQVDTDK+64kax4E9pvDZC6B/HGse9hOUGWXTjb0WZBAkEAxdAw/14iJIUcE5sz
HDy2R0RmbUQYFjrNgBCi5tnmr1Ay1zHAs1VEF+Rg5IOtCBO50I9jm4WCSwCtN6zF
FoFVAQJAUGfBJDcZIm9ZL6ZPXJrqS5oP/wdLmtFE3hfd1gr7C8oHu7BREWB6h1qu
8c1kPlI4+/qDHWaZtQpJ977mIToJwQJAMcgUHKAm/YPWLgT31tpckRDgqgzh9u4z
e1A0ft5FlMcdFFT8BuWlblHWJIwSxp6YO6lqSuBNiuyPqxw6uVAxAQJAWGxOgn2I
fGkWLLw4WMpkFHmwDVTQVwhTpmMP8rWGYEdYX+k9HeOJyVMrJKg2ZPXOPtybrw8T
PUZE7FgzVNxypQ==
-----END PRIVATE KEY-----
</key>
```

## Configure OpenVPN to authenticate with Radius

Create the file **/etc/pam_radius_auth.conf** and add the following contents to it. RADIUSSERVERIP should be the IP of your USG, 192.168.4.1 in my case. Enter the shared secret you created when you enabled the RADIUS server.

To edit and create the file you can do:

`vi /etc/pam_radius_auth.conf`

To exit the editor you just have to press Esc and then type :wq, this will write and quit the editor.

Do NOT use editor such as Notepad++ because they add a character to identify where the line end and goes to a new line (or at least I presume it is like that). If you use another editor you can check if your file is correct using: `cat /etc/pam_radius_auth.conf`, this will display the content of the file.

```
RADIUSSERVERIP SHAREDSECRET
```

Create the file `/etc/pam.d/openvpn` and add the following contents (same as before to create it, just change the path and the name of the file):

```
auth sufficient pam_radius_auth.so debug
account sufficient pam_permit.so
session sufficient pam_permit.so
```

You should now be able to connect to your USG via OpenVPN using Radius authentication with the username and password you configured in the beginning.

## Connecting to the VPN

#### If you are on Linux:

`sudo apt install network-manager-openvpn -y`

After that, open the network manager and add a connection and select to import a configuration from saved file. Here you will have to choose the .ovpn file.

In the screen after that you just enter your username and password for the user that you created for the RADIUS Server and make sure the gateway is correct. The password for the userkey is not needed.

#### If you are in Windows

Just use the OpenVPN client and import the file, you can download it [here](https://openvpn.net/download-open-vpn/).

## Persist your changes

Files will be overwritten after provisions, reboot and firmware upgrade. In order to persist your changes, you'll need to add a script to the **/config/scripts** directory to copy your files over and schedule a task to complete this after each provision.

Copy the files `pam_radius_auth.conf` and `openvpn` from the previous steps into a new directory in **/config/scripts**. I used **ovpn_radius_config**.

Copy the following script to `/config/scripts/postprovision.sh` (you can use the method mentioned previously to create this file. Do NOT forget the first line as I did the first time):

```
#!/bin/vbash
readonly logFile="/var/log/postprovision.log"

source /opt/vyatta/etc/functions/script-template

echo "$(date) - Beginning post provision steps" >> ${logFile}
#restore the ssmtp configuration

cp -f /config/scripts/ovpn_radius_config/pam_radius_auth.conf /etc
cp -f /config/scripts/ovpn_radius_config/openvpn /etc/pam.d/openvpn

#the following lines remove the postprovision scheduled task
#do not modify below this line

configure >> ${logFile}
delete system task-scheduler task postprovision >> ${logFile}
commit >> ${logFile}
save >> ${logFile}
#exit

#end no edit

exit

echo "$(date) - Finished post provision steps" >> ${logFile}
```

Mark your script executable:
`sudo chmod +x /config/scripts/postprovision.sh`

Also copy the following script to `/config/scripts/post-config.d/postreboot.sh`:

```
#!/bin/vbash
readonly logFile="/var/log/postreboot.log"

source /opt/vyatta/etc/functions/script-template

#restore the radius configuration
echo "Copying PAM and OpenVPN config files" >> ${logFile}
cp -f /config/scripts/ovpn_radius_config/pam_radius_auth.conf /etc
cp -f /config/scripts/ovpn_radius_config/openvpn /etc/pam.d/openvpn
```

Mark your script executable: 

`sudo chmod +x /config/scripts/post-config.d/postreboot.sh`

Update your config:

```
configure
set system task-scheduler task postprovision executable path "/config/scripts/postprovision.sh"
set system task-scheduler task postprovision interval 3m
commit
save
exit
```

Unfortunately this is not enough and you will have to configure a file on the controller so every time the USG get provisioned everything will work fine.

This part of the guide is ONLY for Linux users, who will also have to check what is the path in their SO (look [here](https://help.ui.com/hc/en-us/articles/115004872967)).

I'm using MX Linux which has the same path as Debian.

Enter your controller and navigate to **/var/lib/unifi/sites/default/**, here you should find a file named **config.gateway.json**. If the folder doesn't exist, just upload an image to your controller site. To do it go in the **Map** section and switch to **Floorplan** instead of  **Topology**. Here you can add a new floorplan and just add an image. You should now find the directory, and if you don't, just restart the controller.

Now we will need to edit the **config.gateway.json** file, you can use my configuration if you have followed the same process or do a diff between the file saved with the first command and a second file that you can create with: 

`sudo mca-ctrl -t dump-cfg > /config/config-withvpn.json`

My **config.gateway.json** file:

```
{
        "firewall": {
                "ipv6-name": {
                        "wan_local-6": {
                                "default-action": "drop",
                                "rule": {
                                        "20": {
                                                "action": "accept",
                                                "description": "Allow OpenVPN clients in",
                                                "destination": {
                                                        "port": "1194"
                                                },
                                                "log": "disable",
                                                "protocol": "udp"
                                        }
                                }
                        }
                },
                "name": {
                        "WAN_LOCAL": {
                                "default-action": "drop",
                                "description": "packets from internet to gateway",
                                "rule": {
                                        "20": {
                                                "action": "accept",
                                                "description": "Allow OpenVPN clients in",
                                                "destination": {
                                                        "port": "1194"
                                                },
                                                "log": "disable",
                                                "protocol": "udp"
                                        }
                                }
                        }
                }
        },
        "interfaces": {
                "openvpn": {
                        "vtun0": {
                                "encryption": "aes128",
                                "mode": "server",
                                "openvpn-option": [
                                        "--keepalive 8 30",
                                        "--comp-lzo",
                                        "--duplicate-cn",
                                        "--user nobody --group nogroup",
                                        "--plugin /usr/lib/openvpn/openvpn-auth-pam.so openvpn",
                                        "--client-cert-not-required --username-as-common-name",
                                        "--verb 1",
                                        "--proto udp6",
                                        "--port 1194",
                                        "--push redirect-gateway def1",
                                        "--push dhcp-option DNS 8.8.8.8",
                                        "--push dhcp-option DNS 8.8.4.4"
                                ],
                                "server": {
                                        "subnet": "10.72.1.0/24"
                                },
                                "tls": {
                                        "ca-cert-file": "/config/auth/keys/ca.crt",
                                        "cert-file": "/config/auth/keys/server.crt",
                                        "dh-file": "/config/auth/keys/dh2048.pem",
                                        "key-file": "/config/auth/keys/server.key"
                                }
                        }
                }
        },
        "service": {
                "nat": {
                        "rule": {
                                "5010": {
                                        "description": "Masquerade for WAN",
                                        "outbound-interface": "eth0",
                                        "type": "masquerade"
                                }
                        }
                }
        },
        "system": {
                "task-scheduler": {
                        "task": {
                                "postprovision": {
                                        "executable": {
                                                "path": "/config/scripts/postprovision.sh"
                                        },
                                        "interval": "3m"
                                }
                        }
                }
        }
}
```



## Troubleshoot

If you find you have trouble connecting, check your USG logs with **show log | grep openvpn**

## Changelog

- 20/06/2020: First release
